package com.java.felight.dao;

import org.springframework.stereotype.Repository;

import com.java.felight.model.Student;

@Repository("studentDao")
public class StudentDaoImpl extends AbstractDao<Integer, Student>implements StudentDao {

	public void saveStudent(Student student) {  
		persist(student);
	}

	
}
   