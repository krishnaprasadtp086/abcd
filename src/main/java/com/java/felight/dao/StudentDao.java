package com.java.felight.dao;

import com.java.felight.model.Student;

public interface StudentDao {
 void saveStudent(Student student);
}
