package com.java.felight.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.java.felight.dao.StudentDao;
import com.java.felight.model.Student;
@Service("studentService")
@Transactional
public class StudentServiceImpl implements StudentService {
@Autowired
private StudentDao dao;
	public void saveStudent(Student student) {
		
     dao.saveStudent(student);
	}

}
