package com.java.felight.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.java.felight.model.Student;
import com.java.felight.service.StudentService;
@Controller
@RequestMapping("/")
public class MyController {
	
	@Autowired
	StudentService service;
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String newStudent(ModelMap model) {
		Student student = new Student();
		
		model.addAttribute("student", student);
		model.addAttribute("edit", false); 
		 
		
		return "registration";
	}
	@RequestMapping(value = { "/" }, method = RequestMethod.POST)
	public String saveStudent(@Valid Student student, BindingResult result,
			ModelMap model) {
		
		service.saveStudent(student);
		model.addAttribute("success", "Student " + student.getName() + " registered successfully");
		return "success";
	}


}
