package com.java.felight.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="student")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "phone_number", nullable = false)
	private String phone_number;
	@Column(name = "DOB", nullable = false)
	private String dob;
	@Column(name = "address", nullable = false)
	private String address;
	
	public Student() { 
		super();
		// TODO Auto-generated constructor stub
	}
	public Student(int id, String name, String phone_number, String dob, String address) {
		super();
		this.id = id;
		this.name = name;
		this.phone_number = phone_number;
		this.dob = dob;
		this.address = address; 
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public String getDob() {
		return dob;
	}
	public String getAddress() {
		return address;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", phone_number=" + phone_number + ", dob=" + dob + ", address="
				+ address + "]";
	}
	
	
	
	
	
}
